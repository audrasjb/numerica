<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body>
<div class="affichageF">
    <div class="pure-g">
        <div class="pure-u-1 contact column">
            <!-- contact section -->
            <?php echo esc_html(get_theme_mod('contact_code')); ?>
        </div>
    </div>
</div>

    <!-- title -->
    <div class="affichageF">
        <div class="pure-g">
        <div class="pure-u-1 pure-u-md-1 pure-u-lg-1-4 column">
                <div class="blog-header">
                    <a href="<?php esc_url(home_url('url')); ?>"></a>
                    <?php if (function_exists('the_custom_logo')) {
                        the_custom_logo();
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- navigation -->
        <div class="pure-u-1 pure-u-md-1 pure-u-lg-3-4 column-flex menu-smart">
                <div class="pure-menu pure-menu-horizontal">
                    <div class="custom-menu">
                        <ul class="pure-menu-list">
                            <li class="pure-menu-item">

                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'primary',
                                    'menu_id'         => 'main_menu',
                                    'container' => 'div',
                                    'container_class' => 'custom-menu-container',
                                    'menu_class' => 'pure-menu-list',
                                    'depth' => '0'
                                ));
                                ?>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </div>