��                              
   =     H     U     j  &   z  %   �     �     �     �     �     �     �     	                )     6     J     \  #   d     �     �  8   �  
   �  
   �     �  V   �  &   H     o  %   �    �  &   �     �                3  *   C  /   n     �     �     �     �     �  &   �     �     �     	     	     *	     D	  	   V	  &   `	     �	     �	  ?   �	  
   �	     �	     �	  Q   �	     O
     _
  %   t
   A comment on &ldquo;%1$s&rdquo; Arrow icon Back To Home Comments are closed. Contact section Enter the text for the contact section Enter the text for the footer section Footer section Huge Large Next Numerica Oups... Page not found... Pages Patrice Andreani Previous Primary menu Proudly powered by  Read More &raquo; Regular Simple, lightweight WordPress theme Small Theme There's an error, the requested page haven't been found. Title menu With arrow With asterisk comments title%1$s comments on &ldquo;%2$s&rdquo; %1$s comments on &ldquo;%2$s&rdquo; comments titleA comment %1$s comments https://andre-ani.fr https://gitlab.com/andre0ani/numerica Project-Id-Version: numerica
POT-Creation-Date: 2020-11-18 11:35+0100
PO-Revision-Date: 2023-12-27 10:31+0000
Last-Translator: Numericatous
Language-Team: Français
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _e;__;_nx;esc_html_e;esc_html;esc_html__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: footer.php
X-Poedit-SearchPath-1: functions.php
X-Poedit-SearchPath-2: header.php
X-Poedit-SearchPath-3: index.php
X-Poedit-SearchPath-4: page.php
X-Poedit-SearchPath-5: page-single.php
X-Poedit-SearchPath-6: 404.php
X-Poedit-SearchPath-7: comments.php
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.6.6; wp-6.4.2 Un commentaire sur  &ldquo;%1$s&rdquo; Icône flèche Retour à l'accueil Les commentaires sont fermés. Section contact Entrez votre texte pour la section contact Entrez votre texte pour la section pied de page Section pied de page Enorme Large Suivant Numerica Oups... La page n'a pas été trouvée Pages Patrice Andreani Précédent Menu principal Fièrement propulsé par  Lire plus &raquo; Régulier Thème simple et léger pour WordPress Petit Thème Il y a eu une erreur, la page demandée n'a pas été trouvée. Menu titre Avec flèche Avec astérisque %1$s commentaires sur &ldquo;%2$s&rdquo; %1$s commentaires sur &ldquo;%2$s&rdquo; Un commentaire  https://andre-ani.fr https://gitlab.com/andre0ani/numerica 