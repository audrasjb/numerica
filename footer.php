<!-- footer -->
<footer>
    <div class="pure-g footer2">

        <div class="pure-u-1-4 pure-u-md-1-2 pure-u-lg-1-4 column">
            <?php if (is_active_sidebar('widget-footer-1')) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar('widget-footer-1'); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="pure-u-1-4 pure-u-md-1-2 pure-u-lg-1-4 column">
            <?php if (is_active_sidebar('widget-footer-2')) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar('widget-footer-2'); ?>
                </div>
            <?php endif; ?>
        </div>


        <div class="pure-u-1-4 pure-u-md-1-2 pure-u-lg-1-4 column">
            <?php if (is_active_sidebar('widget-footer-3')) : ?>
                <div id="secondary" class="widget-area" role="complementary">
                    <?php dynamic_sidebar('widget-footer-3'); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="pure-u-1-4 pure-u-md-1-2 pure-u-lg-1-4 column">
            <?php if (is_active_sidebar('widget-footer-4')) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar('widget-footer-4'); ?>
                </div>
            <?php endif; ?>
        </div>

    </div>

    <div class="footer2">
        <div class="pure-g">
            <div class="pure-u-1-4 pure-u-md-1-1 pure-u-lg-1-1 column">

                <p class="textF"><?php echo _e('Proudly powered by ', 'numerica') ?> <a href="https://fr.wordpress.org/" target="_blank">Wordpress</a></p>

                <p>&#x1f12f; -
                    <?php echo date_i18n(__('Y', 'numerica')); ?>
                    - <?php echo _e('Theme', 'numerica'); ?> : <a href="https://gitlab.com/andre0ani/numerica" target="_blank"><?php
                                                                                                                                $my_theme = wp_get_theme('numerica');
                                                                                                                                echo $my_theme->get('Name');
                                                                                                                                ?></a>
                </p>
            </div>

            <div class="pure-u-3-4 pure-u-md-1-1 pure-u-lg-1-1 column">
                <p>
                    <?php
                    echo esc_html(get_theme_mod("footer_code"));
                    ?>
                </p>
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>
</body>

</html>