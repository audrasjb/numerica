<!-- page-single -->
<div class="blog-post">
    <div class="presentationArt">
        <h2 class="blog-post-title"><?php the_title(); ?></h2>
    </div>
    <div class="presentationArt2">
        <p class="blog-post-meta">
            <svg width="1em" alt="" height="1em" viewBox="0 0 16 16" class="bi bi-person-square icon" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                <path fill-rule="evenodd" d="M2 15v-1c0-1 1-4 6-4s6 3 6 4v1H2zm6-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
            <?php the_author(); ?>
        </p>

        <?php the_content(); ?>
    </div>
</div>