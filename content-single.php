<!-- content-single -->
<div class="presentationArt title-article">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</div>
<div class="presentationArt2">
    <!-- call content on content-single -->
    <?php the_content(); ?>
</div>