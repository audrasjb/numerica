<?php get_header(); ?>
<!-- index -->
<div class="affichageF">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-3-4 pure-u-lg-3-4 column-flex">
            <!-- call all articles -->
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                    get_template_part('content', get_post_format());
            ?>

                <?php endwhile; ?>

                <div class="nav-previous alignleft"><?php previous_posts_link(__('Previous', 'numerica')); ?></div>
                <div class="nav-next alignright"><?php next_posts_link(__('Next', 'numerica')); ?></div>
            <?php endif;
            ?>
        </div>
        <!-- call sidebar -->
        <div class="pure-u-1 pure-u-md-1-4 pure-u-lg-1-4 column">
            <?php get_sidebar(); ?>
        </div>
        
    </div>

</div>

<?php get_footer(); ?>