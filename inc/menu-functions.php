<?php

function numerica_add_sub_menu_toggle( $output, $item, $depth, $args ) {
	if ( 0 === $depth && in_array( 'menu-item-has-children', $item->classes, true ) ) {
		$output .= '<button class="sub-menu-toggle" aria-expanded="false" onClick="numericaExpandSubMenu(this)">';
		$output .= '<span class="screen-reader-text">' . esc_html__( 'Title menu', 'numerica' ) . '</span>';
		$output .= '</button>';
	}
	return $output;
}
add_filter( 'walker_nav_menu_start_el', 'numerica_add_sub_menu_toggle', 10, 4 );


function numerica_add_menu_description_args( $args, $item, $depth ) {
	$args->link_after = '';
	if ( 0 === $depth && isset( $item->description ) && $item->description ) {
		$args->link_after = '<p class="menu-item-description"><span>' . $item->description . '</span></p>';
	}
	return $args;
}
add_filter( 'nav_menu_item_args', 'numerica_add_menu_description_args', 10, 3 );